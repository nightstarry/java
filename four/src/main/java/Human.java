import java.util.Objects;

public class Human {

    private String firstName;
    private String middleName;
    private String lastName;
    private int age;

    public Human(String firstName, String middleName, String lastName, int age)throws HumanException{
        this.firstName = firstName;
        this.middleName = middleName;
        this.lastName = lastName;
        if (age < 0){
            throw new HumanException("Age < 0!");
        }
        this.age = age;
    }

    public Human(Human human){
        this.firstName = human.firstName;
        this.middleName = human.middleName;
        this.lastName = human.lastName;
        this.age = human.age;
    }

    public Human(){}
    public String getFirstName() {
        return firstName;
    }

    public void setFirstName(String firstName) {
        this.firstName = firstName;
    }

    public String getMiddleName() {
        return middleName;
    }

    public void setMiddleName(String middleName) {
        this.middleName = middleName;
    }

    public String getLastName() {
        return lastName;
    }

    public void setLastName(String lastName) {
        this.lastName = lastName;
    }

    public int getAge() {
        return age;
    }

    public void setAge(int age) throws HumanException {
        if (age < 0){
            throw new HumanException("Age < 0!");
        }
        this.age = age;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        Human human = (Human) o;
        return age == human.age &&
                Objects.equals(firstName, human.firstName) &&
                Objects.equals(middleName, human.middleName) &&
                Objects.equals(lastName, human.lastName);
    }

    @Override
    public int hashCode() {
        return Objects.hash(middleName, firstName, lastName, age);
    }

}
