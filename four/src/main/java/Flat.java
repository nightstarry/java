import java.io.Serializable;
import java.util.List;
import java.util.Objects;

public class Flat  implements Serializable {
    private int number;
    private int square;
    private List<Human> owners;

    public Flat(int number, int square, List<Human> owners) {
        this.number = number;
        this.owners = owners;
        this.square = square;
    }
    public Flat(Flat flat){
        this.number = flat.number;
        this.owners = flat.owners;
        this.square = flat.square;
    }

    public Flat(){}

    public List<Human> getOwners() {
        return owners;
    }

    public void setOwners(List<Human> owners) {
        this.owners = owners;
    }

    public int getNumber() {
        return number;
    }

    public void setNumber(int number) {
        this.number = number;
    }

    public int getSquare() {
        return square;
    }

    public void setSquare(int square) {
        this.square = square;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        Flat flat = (Flat) o;
        return number == flat.number &&
                Double.compare(flat.square, square) == 0 &&
                Objects.equals(owners, flat.owners);
    }

    @Override
    public int hashCode() {
        return Objects.hash(number, square, owners);
    }

    @Override
    public String toString() {
        return "Flat{" +
                "number=" + number +
                ", square=" + square +
                ", owners=" + owners +
                '}';
    }
}
