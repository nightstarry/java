import java.lang.reflect.Method;
import java.util.ArrayList;
import java.util.List;

public class ReflectionDemo {

    //Дан список объектов произвольных типов. Найдите количество элементов списка, которые
    //являются объектами типа Human или его подтипами.
    public static int countHuman(List<Object> list) {
        int count = 0;

        for (Object item : list) {
            if (item instanceof Human) {
                count++;
            }
        }
        return count;
    }

    //Для объекта получить список имен его открытых методов.
    public static List<String> getPublicMethods(Object obj) {
        Class cl = obj.getClass();
        List<String> result = new ArrayList<>();

        for (Method method : cl.getMethods()) {
            result.add(method.getName());
        }
        return result;
    }

    //Для объекта получить список (в виде списка строк) имен всех его суперклассов до класса
    //Object включительно.
    public static List<String> getSuperClass(Object obj) {
        Class cl = obj.getClass().getSuperclass();
        List<String> result = new ArrayList<>();
        while (!(cl == null)) {
            result.add(cl.getSimpleName());
            cl = cl.getSuperclass();
        }
        return result;
    }

}
