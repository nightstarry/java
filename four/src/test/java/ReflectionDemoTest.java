import org.junit.jupiter.api.Test;

import java.util.ArrayList;
import java.util.Collections;
import java.util.List;

import static org.junit.jupiter.api.Assertions.assertEquals;

public class ReflectionDemoTest {

    @Test()
    public void TestCountHuman()throws HumanException{
        List<Object> list = new ArrayList<>();

        list.add(new Human("Иван", "Иванович", "Иванов", 21));
        list.add(new Human("Иван", "Иванович", "Иванов", 22));
        list.add(new Human("Иван", "Иванович", "Сидоров", 20));
        list.add(new Student("Иван", "Олегович", "Сидоров",19));
        list.add(new Student("Иван", "Олегович", "Иванов",24));
        list.add(new Flat());

        assertEquals(ReflectionDemo.countHuman(list), 5);
    }

    @Test()
    public void TestPublicMethods()throws HumanException{
        Human human = new Human();
        List<String> result = new ArrayList<>();

        result.add("wait");
        result.add("wait");
        result.add("wait");
        result.add("toString");
        result.add("getClass");
        result.add("notify");
        result.add("notifyAll");
        result.add("getFirstName");
        result.add("setFirstName");
        result.add("getMiddleName");
        result.add("setMiddleName");
        result.add("getLastName");
        result.add("setLastName");
        result.add("getAge");
        result.add("setAge");
        result.add("equals");
        result.add("hashCode");

        List<String> actual=ReflectionDemo.getPublicMethods(human);
        Collections.sort(result);
        Collections.sort(actual);
        assertEquals(result,actual);

    }

    @Test()
    public void TestSuperClass()throws HumanException{

        List<String> result = new ArrayList<>();

        result.add("Human");
        result.add("Object");

        Student student = new Student();

        List<String> actual=ReflectionDemo.getSuperClass(student);

        Collections.sort(result);
        Collections.sort(actual);

        assertEquals(result, actual);
    }
}
