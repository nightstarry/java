public interface IInvertableMatrix extends IMatrix {
    IInvertableMatrix inverseMatrix(/*Matrix matrix*/) throws DeterminantZeroException;
}
