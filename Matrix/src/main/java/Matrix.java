import java.io.Serializable;
import java.util.Arrays;


public class Matrix implements IMatrix, Serializable {

    private int N;
    private double[] matrix;
    private double determinant;
    protected boolean flag;

    public Matrix(int N) throws IllegalArgumentException {
        if (N <= 0) {
            throw new IllegalArgumentException("Размерность меньше либо = 0");
        }
        this.N = N;
        matrix = new double[N * N];
        flag = false;
    }

    public int getSize() {
        return N;
    }

    public double getElem(int i, int j) throws IllegalArgumentException {
        if (i < 0 || j < 0 || i > N - 1 || j > N - 1) {
            throw new IllegalArgumentException("Выход за границу!");
        }
        return matrix[i * N + j];
    }

    public void changeElem(int i, int j, double x) throws IllegalArgumentException {
        if (i < 0 || j < 0 || i > N - 1 || j > N - 1) {
            throw new IllegalArgumentException("Выход за границу!");
        }
        matrix[i * N + j] = x;
        flag = false;
        //return matrix[i * N + j];
    }

    public void changeElemByIndex(int i, double x) {
        matrix[i] = x;
        flag = false;
    }

    public Matrix(Matrix copy) {
        this.N = copy.N;
        matrix = new double[N * N];
        for (int i = 0; i < N * N; i++) {
            this.matrix[i] = copy.matrix[i];
        }
        flag=false;
    }


    public double determinant() {
        if (flag) {
            return determinant;
        }

        double det = 1;
        int h = 0;
        double[] buf = new double[N * N];
        for (int i = 0; i < N * N; i++) {
            buf[i] = matrix[i];
        }

        for (int i = 1; i < N; i++) {
            if (Math.abs(buf[h] - 0.0) < 10e-5) {
                for (int k = 0; k < N - i + 1; k++) {
                    if (buf[k * N + h] != 0.0) {
                        for (int t = 0; t < N - i + 1; t++) {
                            buf[h + t] = buf[h + t] + buf[k * N + h + t];
                        }
                        break;
                    }
                    if (k == N - 1) {
                        det = 0;
                    }
                }
            }
            if (Math.abs(det - 0.0) < 10e-5) {
                break;
            }
            if(buf[h] != 0.0) {
                for (int q = i; q < N; q++) {
                    double buf2 = buf[h + (q - i + 1) * N];
                    for (int j = 0; j < N; j++) {
                        buf[j + N * q] = buf[j + N * q] - ((buf[j + (i - 1) * N] / buf[h]) * buf2);
                    }
                }
            }
            det *= buf[h];
            h += (N + 1);
        }
        det *= buf[h];

        this.determinant = det;
        flag = true;
        return determinant;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        Matrix matrix1 = (Matrix) o;
        return N == matrix1.N && Arrays.equals(matrix, matrix1.matrix);
    }

    @Override
    public int hashCode() {
        int result = N;
        result = 31 * result + Arrays.hashCode(matrix);
        return result;
    }

    public String toString() {
        return "N =" + N + ", matrix = " + Arrays.toString(matrix);
    }
}