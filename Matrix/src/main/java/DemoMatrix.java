import java.io.*;
import java.util.Scanner;
import java.util.Locale;
import java.io.FileNotFoundException;

public class DemoMatrix{

    public static double sumElem(Matrix matrix) {
        double sum = 0;
        for (int i = 0; i < matrix.getSize(); i++) {
            for (int j = 0; j < matrix.getSize(); j++) {
                sum += matrix.getElem(i, j);
            }
        }
        return sum;
    }

    public static void writeStream(Matrix matrix, Writer file) throws IOException {
        for (int i = 0; i < matrix.getSize(); i++) {
            for (int j = 0; j < matrix.getSize(); j++) {
                file.write(matrix.getElem(i, j) + " ");
            }
            file.write(System.getProperty("line.separator"));
        }
        file.close();
    }

    public static void readStream(Matrix matrix, Scanner file) throws IllegalArgumentException {
        for (int i = 0; i < matrix.getSize(); i++) {
            for (int j = 0; j < matrix.getSize(); j++) {
                matrix.changeElem(i, j, file.nextDouble());
            }
        }
    }

    public static void main(String[] avg) throws IllegalArgumentException {
        Matrix matrix = null;
        int N = 3;

        try {
            matrix = new Matrix(N);
            for (int i = 0; i < N; i++) {
                matrix.changeElem(i, i, i + 25);
            }
        } catch (IllegalArgumentException e) {
            System.err.println("Размер должен быть положительный!");
        }

        InvertableMatrix invMatrix = null;

        try {
            invMatrix = new InvertableMatrix(matrix);
        } catch (DeterminantZeroException e){
            System.err.println("Определитель = 0!");
        }

        Scanner s = null;

        try {
            s = new Scanner(new FileInputStream("C:\\Users\\Aser\\Desktop\\универ\\4 СЕМЕСТР\\Matrix\\1.txt"));
            s.useLocale(Locale.US);
        } catch (FileNotFoundException e) {
            e.printStackTrace();
        }
        //считываем матрицу из файла 1.txt

        try {
            readStream(matrix, s);
            s = new Scanner(new FileInputStream("C:\\Users\\Aser\\Desktop\\универ\\4 СЕМЕСТР\\Matrix\\1.txt"));
            readStream(invMatrix, s);
        } catch (IOException e) {
            e.printStackTrace();
        }

        //выполняем методы классов

        System.out.println("Размер: " + matrix.getSize());
        System.out.println("Определитель :" + matrix.determinant());

        try {
            System.out.println("Взять элемент : " + matrix.getElem(1, 0));
        } catch (IllegalArgumentException e) {
            System.err.println("Выход за границу матрицы!");
        }

        System.out.println(matrix.toString());

        try{
            System.out.println(invMatrix.inverseMatrix().toString());
        } catch (DeterminantZeroException e){
        }

        InvertableMatrix newMatr = null;

        try {
            newMatr = (InvertableMatrix) invMatrix.inverseMatrix();
        } catch (DeterminantZeroException e){
            e.printStackTrace();
        }

        System.out.println("Equals: " + matrix.equals(newMatr));
        System.out.println("HashCode: " + matrix.hashCode());

        //записываем обратную матрицу в файл 2.txt

        try {
            BufferedWriter write = new BufferedWriter(new FileWriter("C:\\Users\\Aser\\Desktop\\универ\\4 СЕМЕСТР\\Matrix\\2.txt"));
            writeStream(newMatr, write);
        } catch (IOException e) {
            e.printStackTrace();
        }
        System.out.println("Сумма элементов : " + sumElem(matrix));


        //СЕРИАЛИЗАЦИЯ

        try(ObjectOutputStream oos = new ObjectOutputStream(new FileOutputStream("C:\\Users\\Aser\\Desktop\\универ\\4 СЕМЕСТР\\Matrix\\3.txt")))
        {
            oos.writeObject(matrix);
        }
        catch(Exception e){
            System.out.println(e.getMessage());
        }

        //ДЕСЕРИАЛИЗАЦИЯ

        try
        {ObjectInputStream ois = new ObjectInputStream(new FileInputStream("C:\\Users\\Aser\\Desktop\\универ\\4 СЕМЕСТР\\Matrix\\3.txt"));
            Matrix p = (Matrix)ois.readObject();
            System.out.println(matrix.equals(p));
            try {
                BufferedWriter write = new BufferedWriter(new FileWriter("C:\\Users\\Aser\\Desktop\\универ\\4 СЕМЕСТР\\Matrix\\4.txt"));
                writeStream(p, write);
            } catch (IOException e) {
                e.printStackTrace();
            }
        }
        catch(Exception ex){
            System.out.println(ex.getMessage());
        }


    }
}
