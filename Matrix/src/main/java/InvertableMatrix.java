public class InvertableMatrix extends Matrix implements IInvertableMatrix {

    public InvertableMatrix(int N) throws IllegalArgumentException {
        super(N);
        for (int i = 0; i < N; i++) {
            super.changeElem(i, i, 1.0);
        }
    }

    public InvertableMatrix(Matrix matrix) throws DeterminantZeroException {
        super(matrix);
        if (Math.abs(this.determinant()) == 0) {
            throw new DeterminantZeroException("Определитель равен нулю!");
        }
    }

    public IInvertableMatrix inverseMatrix() throws DeterminantZeroException {
        InvertableMatrix copy = new InvertableMatrix(this);
        Matrix p = new Matrix(this.getSize() - 1);
        InvertableMatrix o = new InvertableMatrix(this.getSize());
        double det = this.determinant();
        int index = 0;

        for (int i = 0; i < this.getSize(); i++) {
            for (int j = 0; j < this.getSize(); j++) {
                for (int k = 0; k < this.getSize(); k++) {
                    for (int l = 0; l < this.getSize(); l++) {
                        if (!(k == i) && !(l == j)) {
                            p.changeElemByIndex(index, copy.getElem(k, l));
                            index++;
                        }
                    }
                }
                index = 0;
                flag = (false);
                double ok = Math.pow(-1, i + j) * p.determinant();
                 o.changeElem(j, i, ok);
            }
        }
        for (int k = 0; k < copy.getSize(); k++) {
            for (int l = 0; l < copy.getSize(); l++) {
                double tmp = o.getElem(k, l) / det;
                o.changeElem(k, l, tmp);
            }
        }
        return o;
    }
}
