public interface IMatrix {
    double getElem(int i, int j) throws IllegalArgumentException;
    void changeElem(int i, int j, double x) throws IllegalArgumentException;
    double determinant();
}