public class DeterminantZeroException extends Exception {
    public DeterminantZeroException() {
    }

    public DeterminantZeroException(String message) {
        super(message);
    }
}
