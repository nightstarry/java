import org.junit.Test;

import static org.junit.Assert.*;
import static org.junit.jupiter.api.Assertions.assertEquals;

public class LambdaTest{


    @Test
    public void getLengthTest() {
        String s = "Длина строки";
        assertEquals(s.length(), LambdaRunner.executeFunction(LambdaDemo.getLength, s).intValue());
    }

    @Test
    public void firstSymbolOfStringTest() {
        String s = "Первый символ строки";
        assertEquals('П', LambdaRunner.executeFunction(LambdaDemo.firstSymbolOfString , s).charValue());
    }

    @Test
    public void checkSpaceTest() {
        String s = "строка";
       assertTrue(LambdaRunner.executeFunction(LambdaDemo.checkSpace, s));
    }

    @Test
    public void getCountOfWordsTest() {
        String s = "Слова, разделены, запятыми";
        assertEquals(3, LambdaRunner.executeFunction(LambdaDemo.getCountOfWords, s).intValue());
    }

    @Test
    public void ageOfHumanTest() throws HumanException {
        Human man = new Human("Иван", "Иванович", "Иванов", 21, Gender.MALE);
        assertEquals(21, LambdaRunner.executeFunction(LambdaDemo.ageOfHuman, man).intValue());
    }

    @Test
    public void ageOfStudentTest() throws HumanException {
        Student man = new Student("Иван", "Иванович", "Иванов", 21, Gender.MALE, "university", "faculty", "specialization");
        assertEquals(21, LambdaRunner.executeFunction(LambdaDemo.ageOfHuman, man).intValue());
    }

    @Test
    public void compareLastNameTest1() throws HumanException {
        Human manOne = new Human("Иван", "Иванович", "Иванов", 21, Gender.MALE);
        Human manTwo = new Human("Иван", "Олегович", "Иванов", 71, Gender.MALE);
        assertTrue(LambdaRunner.executeBiPredicate(LambdaDemo.compareLastName, manOne, manTwo));
    }

    @Test
    public void compareLastNameTest2() throws HumanException {
        Student manOne = new Student("Иван", "Иванович", "Иванов", 21, Gender.MALE, "university", "faculty", "specialization");
        Student manTwo = new Student("Иван", "Олегович", "Иванов", 71, Gender.MALE, "university", "faculty", "specialization");
        assertTrue(LambdaRunner.executeBiPredicate(LambdaDemo.compareLastName, manOne, manTwo));
    }

    @Test
    public void getFIOTest1() throws HumanException {
        Human man = new Human("Иван", "Иванович", "Иванов", 21, Gender.MALE);
        assertEquals("Иванов Иван Иванович", LambdaRunner.executeFunction(LambdaDemo.getFIO, man));
    }

    @Test
    public void getFIOTest2() throws HumanException {
        Student man = new Student("Иван", "Иванович", "Иванов", 21, Gender.MALE, "university", "faculty", "specialization");
        assertEquals("Иванов Иван Иванович", LambdaRunner.executeFunction(LambdaDemo.getFIO, man));
    }

    @Test
    public void makeOlderTest() throws HumanException {
        Human man = new Human("Иван", "Иванович", "Иванов", 21, Gender.MALE);
        Human newMan = new Human(man.getLastName(), man.getFirstName(), man.getMiddleName(), man.getAge() + 1, man.getGender());
        assertEquals(newMan, LambdaRunner.executeFunction(LambdaDemo.makeOlder, man));
    }

    @Test
    public void checkMaxAgeTest() throws HumanException {
        Human humanOne = new Human("Иван", "Иванович", "Иванов", 21, Gender.MALE);
        Human humanTwo = new Human("Иван", "Олегович", "Иванов", 27, Gender.MALE);
        Human humanThree = new Human("Олег", "Петрович", "Иванов", 8, Gender.MALE);
        Human[] array = {humanOne, humanTwo, humanThree};
        int maxAge = 28;
        assertTrue(LambdaRunner.executeBiPredicate(LambdaDemo.checkMaxAge, array, maxAge));
    }
}