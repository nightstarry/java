import java.util.Objects;

public class Student extends Human {
    private String university;
    private String faculty;
    private String specialization;

    public Student(){}

    public Student(String firstName, String middleName, String lastName, int age, Gender gender, String university, String faculty, String specialization) throws HumanException {
        super(firstName, middleName, lastName, age, gender);
        this.university = university;
        this.faculty = faculty;
        this.specialization = specialization;
    }

    public Student(String firstName, String middleName, String lastName, int age, Gender gender) throws HumanException {
        super(firstName, middleName, lastName, age, gender);
    }

    public String getSpecialization() {
        return specialization;
    }

    public String getUniversity() {
        return university;
    }

    public String getFaculty() {
        return faculty;
    }

    public void setUniversity(String university) {
        this.university = university;
    }

    public void setSpecialization(String specialization) {
        this.specialization = specialization;
    }

    public void setFaculty(String Faculty) {
        faculty = Faculty;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        Student student = (Student) o;
        return Objects.equals(university, student.university) &&
                Objects.equals(faculty, student.faculty) &&
                Objects.equals(specialization, student.specialization);
    }

    @Override
    public int hashCode() {
        return Objects.hash(university, faculty, specialization);
    }
}
