import java.util.Objects;

public class Human {
    private String firstName, middleName, lastName;
    private int age;
    private Gender gender;

    public Human(Human human){}

    public Human(String firstName, String middleName, String lastName, int age, Gender gender) throws HumanException {
        this.firstName = firstName;
        this.middleName = middleName;
        this.lastName = lastName;
        if(age <= 0){throw new HumanException("age <= 0");}
        this.age = age;
        this.gender = gender;
    }

    public Human() {
    }

    public Gender getGender() {
        return gender;
    }

    public int getAge() {
        return age;
    }

    public String getLastName() {
        return lastName;
    }

    public String getMiddleName() {
        return middleName;
    }

    public String getFirstName() {
        return firstName;
    }

    public void setGender(Gender gender) {
        this.gender = gender;
    }

    public void setFirstName(String firstName) {
        this.firstName = firstName;
    }

    public void setMiddleName(String middleName) {
        this.middleName = middleName;
    }

    public void setLastName(String lastName) {
        this.lastName = lastName;
    }

    public void setAge(int age) {
        this.age = age;
    }

    public String getHumanName(){
        return getLastName()+" "+getFirstName()+" "+getMiddleName();
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        Human human = (Human) o;
        return age == human.age &&
                Objects.equals(firstName, human.firstName) &&
                Objects.equals(middleName, human.middleName) &&
                Objects.equals(lastName, human.lastName) &&
                gender == human.gender;
    }

    @Override
    public int hashCode() {
        return Objects.hash(firstName, middleName, lastName, age, gender);
    }
}
