import java.util.function.BiPredicate;
import java.util.function.Function;

public class LambdaRunner {
    public static <T, V> V executeFunction(Function<T, V> function, T argument) {
        return function.apply(argument);
    }

    public static <T, V> boolean executeBiPredicate(BiPredicate<T, V> predicate, T argumentOne, V argumentTwo) {
        return predicate.test(argumentOne, argumentTwo);
    }
}
