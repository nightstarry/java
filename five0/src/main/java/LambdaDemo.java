import java.util.function.BiPredicate;
import java.util.function.Function;


public class LambdaDemo {
    // 1. для строки символов получить ее длину
    public static final Function<String, Integer> getLength = string -> string.length();

    // 2. для строки символов получить ее первый символ, если он существует, или null иначе
    public static final Function<String, Character> firstSymbolOfString = s -> s!=null&&!"".equals(s) ? s.charAt(0) : null;

    // 3. для строки проверить, что она не содержит пробелов
    public static final Function<String, Boolean> checkSpace = s -> s==null ||!s.equals("") && !s.contains(" ");

    // 4. слова в строке разделены запятыми, по строке получить количество слов в ней
    public static final Function<String, Integer> getCountOfWords = s -> s.equals("") ? 0 : s.split(",").length;

    // 5. по человеку получить его возраст
    public static final Function<? super Human, Integer> ageOfHuman = Human::getAge;

    // 6. по двум людям проверить, что у них одинаковая фамилия
    public static final BiPredicate<? super Human, ? super Human> compareLastName = (h1, h2) ->
            h1.getLastName().equals(h2.getLastName());

    // 7. получить фамилию, имя и отчество человека в виде одной строки (разделитель пробел)
    public static final Function<? super Human, String> getFIO = human -> human.getLastName()+" " + human.getFirstName() + " " + human.getMiddleName();

    // 8. сделать человека старше на один год
    public static final Function<Human, Human> makeOlder = h -> {
        try {
            return new Human(h.getLastName(), h.getFirstName(),
                    h.getMiddleName(), h.getAge() + 1, h.getGender());
        } catch (HumanException e) {
            e.printStackTrace();
        } return null;
    };

    // 9. по трем людям и заданному возрасту maxAge проверить, что все три человека моложе maxAge
    public static final BiPredicate<Human [], Integer> checkMaxAge = (human, maxAge) ->
            maxAge >  human[0].getAge() && maxAge > human[1].getAge() && maxAge > human[2].getAge();

}