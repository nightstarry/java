
import java.lang.Exception;

public class GuadraticTrinomial
{
    private double[]coefs;
    public GuadraticTrinomial(){}
    public GuadraticTrinomial(double ... coefs)
    {
        if(coefs[0] == 0){throw new IllegalArgumentException(" Коэффициент при х^2 не может быть равен 0");
        }
        else {
            this.coefs = coefs;}
    }

    public double[] value()throws Exception
    {
        double d = coefs[1]*coefs[1] - 4*coefs[0]*coefs[2];
        double x1 , x2;
        double[] array = new double[2];
        x1 = (- coefs[1] + Math.sqrt(d)) / (2*coefs[0]);
        x2 = (- coefs[1] - Math.sqrt(d)) / (2*coefs[0]);
        if (d < 0) {throw new Exception (" Нет вещественных корней.");}
        if (d >= 0) {  array[0] = x1; array[1] = x2;}
        return array;
    }
}

