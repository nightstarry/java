
import org.testng.annotations.Test;
import java.lang.Exception;
import java.util.Arrays;

import static org.junit.jupiter.api.Assertions.assertTrue;
import static org.testng.FileAssert.fail;

public class GuadraticTrinomialTest {
    private GuadraticTrinomial x;

    @Test(expectedExceptions = IllegalArgumentException.class)
    public void polynomTest1 () throws Exception {
        x = new GuadraticTrinomial(0, 2, -3);
        x.value();
        fail();
    }

    @Test(expectedExceptions = Exception.class)
    public void polynomTest2 () throws Exception {
        x = new GuadraticTrinomial(1, 2, 3);
        x.value();
       fail();
    }

    @Test
    public void polynomTest3() throws Exception {
        x = new GuadraticTrinomial(1, 2, -3);
        double []array = new double [2];
        array[0] = 1;
        array[1] = -3;
        assertTrue(Arrays.equals(array, x.value()));
    }

}


