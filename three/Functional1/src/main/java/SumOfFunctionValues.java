
public class SumOfFunctionValues<T extends IFunction> implements IFunctional<T> {

    private double a = 15;
    private double b = -15;

    public double solve(T function) throws Exception {
        double midle = (function.getB() - function.getA()) / 2;
        return (function.value(function.getA()) + function.value(function.getB()) + function.value(midle));
    }
}

