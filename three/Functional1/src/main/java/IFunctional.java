public interface IFunctional<T extends IFunction>  {
    double solve(T function) throws Exception;
}

