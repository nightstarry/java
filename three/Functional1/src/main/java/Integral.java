public class Integral<T extends IFunction> implements IFunctional <T> {
    private double c;
    private double d;

    public Integral(double c, double d){
        this.c = c;
        this.d = d;
    }
    public double solve(T x) throws Exception {
        if (c < x.getA() || d > x.getB()){
            throw new IllegalArgumentException("Отрезок интегрирования не в ходит в область определения функции");
        }
        else {
            double result = 0;
            double h = (d-c)/1000;
            for (int i = 0; i < 1000; i++) {
                result += x.value(c + h * (i + 0.5));
            }
            result *= h;
            return result;
        }
    }
}
