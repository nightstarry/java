import org.junit.jupiter.api.Test;
import static org.junit.Assert.assertEquals;


public class IntegralTest {
    @Test
    public void checkValue1() throws Exception {
        OnePolynom f1 = new OnePolynom(1, 2, 0, 2);
        Integral I = new Integral(0,1);
        assertEquals(2.5, I.solve(f1), 1e-5);
    }
    @Test
    public void checkValue2() throws Exception {
        OneFunction f2 = new OneFunction(1, 1, 0, 5);
        Integral I = new Integral(0,Math.PI);
        assertEquals(2, I.solve(f2), 1e-5);
    }
    @Test
    public void checkValue3() throws Exception {
        TwoFunction f3 = new TwoFunction(2, 2, 2, 2, 0, 3);
        Integral I = new Integral(0,3);
        assertEquals(3, I.solve(f3), 1e-5);
    }
    @Test
    public void checkValue4() throws Exception {
        ExpFunction f4 = new ExpFunction(1, 2, 0, 5);
        Integral I = new Integral(0,0);
        assertEquals(0, I.solve(f4), 1e-5);
    }
}
