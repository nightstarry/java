import org.junit.Test;
import java.lang.Exception;
import static org.junit.Assert.assertEquals;


public class SumOfFunctionValuesTest {

    @Test
    public void checkValue1() throws Exception {
        OnePolynom f1 = new OnePolynom(1, 2, 0, 6);
        SumOfFunctionValues S = new SumOfFunctionValues();

        assertEquals(15, S.solve(f1), 1e-5);
    }
    @Test
    public void checkValue2() throws Exception {
        OneFunction f2 = new OneFunction(2, 1, 0, 0);
        SumOfFunctionValues S = new SumOfFunctionValues();
        assertEquals(0, S.solve(f2), 1e-5);
    }
    @Test
    public void checkValue3() throws Exception {
        TwoFunction f3 = new TwoFunction(2, 2, 1, 1, 0, 6);
        SumOfFunctionValues S = new SumOfFunctionValues();
        assertEquals(6, S.solve(f3), 1e-5);
    }
    @Test
    public void checkValue4() throws Exception {
        ExpFunction f4 = new ExpFunction(1, 2, 0, 0);
        SumOfFunctionValues S = new SumOfFunctionValues();
        assertEquals(9, S.solve(f4), 1e-5);
    }


}

