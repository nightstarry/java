import java.lang.Exception;

public class GuadraticTrinomial1 {

    private GuadraticTrinomial x;

    public GuadraticTrinomial1(double... coefs) {

        this.x = new GuadraticTrinomial(coefs);
    }

    public double largesRoot() throws Exception {

        double[] array = x.value();
        if (array[0] > array[1]) {
            return array[0];
        }
        if (array[0] < array[1]) {
            return array[1];
        }
        if (array[0] == array[1]) {
            return array[0];
        } else {
            throw new Exception("Нет вещественных корней");
        }
    }
}
