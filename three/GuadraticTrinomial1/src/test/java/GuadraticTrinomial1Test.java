import org.junit.Test;
import java.lang.Exception;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.fail;

public class GuadraticTrinomial1Test {
    private GuadraticTrinomial1 x;
    @Test()
    public void polynomTest1 () throws Exception {
        x = new GuadraticTrinomial1(1, 2, -3);
        assertEquals(1, x.largesRoot(), 0.000001);
    }

    @Test(expected = Exception.class)
    public void polynomTest2 () throws Exception {
        x = new GuadraticTrinomial1(1, 2, 3);
        x.largesRoot();
        fail();
    }
}
