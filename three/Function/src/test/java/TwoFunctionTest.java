import org.junit.Test;
import java.lang.Exception;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.fail;

public class TwoFunctionTest {
    private TwoFunction p;

    @Test()
    public void polynomTest1 () throws Exception {
        p = new TwoFunction(1, 2, 3, 4, -10, 10);
        double x = 2;
        assertEquals(0.4, p.value(x), 0.000001);
    }

    @Test(expected = Exception.class)
    public void polynomTest2 () throws Exception {
        double x = -17;
        p = new TwoFunction(1, 2, 3, 4, -10, 10);
        p.value(x);
        fail();
    }

    @Test(expected = Exception.class)
    public void polynomTest3 () throws Exception {
        double x = -9;
        p = new TwoFunction(0, 2, 3, 4, -10, 10);
        p.value(x);
        fail();
    }
    @Test(expected = IllegalArgumentException.class)
    public void polynomTest4 () throws Exception {
        double x = -1;
        p = new TwoFunction(1, 2, 0, 4, -10, 10);
        p.value(x);
        fail();
    }
    @Test(expected = IllegalArgumentException.class)
    public void polynomTest5 () throws Exception {
        double x = -5;
        p = new TwoFunction(0, 2, 0, 4, -10, 10);
        p.value(x);
        fail();
    }


}
