import org.junit.Test;
import java.lang.Exception;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.fail;

public class OnePolynomTest {
    private OnePolynom p;


    @Test()
    public void polynomTest1 () throws Exception {
        p = new OnePolynom(2, 2, -10, 10);
        double x = 2;
       assertEquals(6, p.value(x), 0.000001);
    }

    @Test(expected = Exception.class)
    public void polynomTest2 () throws Exception {
        double x = 16;
        p = new OnePolynom(1, 2, -10, 10);
        p.value(x);
        fail();
    }

    @Test(expected = IllegalArgumentException.class)
    public void polynomTest3 () throws Exception {
        double x = 4;
        p = new OnePolynom(0, 2, -10, 10);
        p.value(x);
        fail();
    }

}
