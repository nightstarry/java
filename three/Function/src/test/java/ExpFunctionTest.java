import org.junit.Test;
import java.lang.Exception;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.fail;

public class ExpFunctionTest {
    private ExpFunction p;

    @Test()
    public void polynomTest1 () throws Exception {
        p = new ExpFunction(1, 2, -10, 10);
        double x = 2;
        assertEquals(Math.exp(2) + 2, p.value(x), 0.000001);
    }

    @Test(expected = Exception.class)
    public void polynomTest2 () throws Exception {
        double x = -17;
        p = new ExpFunction(1, 2, -10, 10);
        p.value(x);
        fail();
    }

}
