public class OneFunction implements IFunction {
    private double parA;
    private double parB;
    private double a ;
    private double b ;
    private double x;

    public OneFunction(double parA, double parB, double a, double b) {

        this.parA = parA;
        this.parB = parB;
        this.a = a;
        this.b = b;

    }


    public double value(double x) throws Exception {
        if (x >= a && b >= x) {
            return (parA * Math.sin(parB*x));
        }
        throw new Exception("Функция, не определенна в этом аргументе.");
    }

    public double getA() {
        return a;
    }

    public double getB() {
        return b;
    }


}
