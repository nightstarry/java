public class TwoFunction implements IFunction {
    private double parA;
    private double parB;
    private double parC;
    private double parD;
    private double a ;
    private double b ;

    public TwoFunction(double parA, double parB, double parC, double parD,  double a, double b) {

        if (parA == 0 || parC == 0) {
            throw new IllegalArgumentException(" Коэффициент при х не может быть равен 0");
        }
        this.parA = parA;
        this.parB = parB;
        this.parC = parC;
        this.parD = parD;
        this.a = a;
        this.b = b;

    }


    public double getA() {
        return a;
    }

    public double getB() {
        return b;
    }

    public double value(double x) throws Exception {
        if (x >= a && b >= x) {
            if ((parC * x + parD) == 0) {
                throw new Exception("");
            }
            return (parA * x + parB) / (parC * x + parD);
        }
        throw new Exception("Функция, не определенна в этом аргументе.");
    }
}
