public class OnePolynom implements IFunction {

    private double parA;
    private double parB;
    private double a ;
    private double b ;


    public OnePolynom(double parA, double parB, double a, double b) {
        if(parA == 0){throw new IllegalArgumentException(" Коэффициент при х не может быть равен 0");
        }

            this.parA = parA;
            this.parB = parB;
            this.a = a;
            this.b = b;

    }


    public double getA() {
        return a;
    }

    public double getB() {
        return b;
    }

    public double value(double x)throws Exception{
        if(x >= a && b >= x) {
            return (parA * x + parB);
        }
        throw new Exception("Функция, не определенна в этом аргументе.");
    }
}
