public class ExpFunction implements IFunction {
    private double parA;
    private double parB;
    private double a ;
    private double b ;


    public ExpFunction(double parA, double parB,  double a, double b) {

        this.parA = parA;
        this.parB = parB;
        this.a = a;
        this.b = b;

    }


    public double value(double x) throws Exception {
        if(x >= a && b >= x) {
            return (parA * Math.exp(x) + parB);
        }
        throw new Exception("Функция, не определенна в этом аргументе.");
    }

    public double getA() {
        return a;
    }

    public double getB() {
        return b;
    }
}
