public interface IFunction {

    double value(double x) throws Exception;

    double getA();

    double getB();

}
