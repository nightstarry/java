
public class Vector3DArray {

    private Vector3D[] array;

    public  Vector3DArray(int size) {
        array = new Vector3D[size];
        for (int i = 0; i < size; i++) {
            array[i] = new Vector3D();
        }
    }

    public void printArray() {
        for (int i = 0; i < array.length; i++) {
            array[i].PrintVector();
            System.out.println();
        }
    }

    public double LengthVector3DArray() {
        return array.length;
    }


    public void setVector(int c, Vector3D one) {
        array[c] = one;
    }


    public double MaxLengthVector(){
        double max = 0;
        for (int i = 0; i < array.length; i++) {
            double length =  array [i].LengthVec();
            if(max<length){max = length;}
        }
        return max;
    }

    public Vector3D sumVectors() {
        Vector3DProcessor processor = new Vector3DProcessor();
        Vector3D result = new Vector3D();
        for (int i = 0; i < array.length; i++) {
            result = processor.sumVec(result, array[i]);
        }
        return result;
    }


    public int SearchVec(Vector3D one){
        int ind = -1;
        for(int i=0; i<array.length; i++){
            if( Math.abs(array[i].getX() - one.getX()) <= 5e-10 && Math.abs(array[i].getY() - one.getY()) <= 5e-10 && Math.abs(array[i].getZ() - one.getZ()) <= 5e-10  ) { ind =  i; }
        } return ind;
    }


    public Vector3D LinearCombination(double[]arr){

        if(array.length != arr.length){
            return new Vector3D( 0, 0, 0);
        }
        Vector3D result = new Vector3D();
        Vector3DProcessor processor = new Vector3DProcessor();
        for (int i = 0; i < array.length; i++) {
            double x = arr[i] * array[i].getX();
            double y = arr[i] * array[i].getY();
            double z = arr[i] * array[i].getZ();
            Vector3D vector = new Vector3D(x, y, z);
            result = processor.sumVec(result, vector);

        }
        return result;

    }

    public Point3D[] pointArray(Point3D p, int size) {
        Point3D arrayPoint[] = new Point3D[size];
        for (int i = 0; i < array.length; i++) {
            double a = p.getX() + array[i].getX();
            double b = p.getY() + array[i].getY();
            double  c = p.getZ() + array[i].getZ();
            Point3D point = new Point3D(a, b, c);
            arrayPoint[i] = point;
        }
        return arrayPoint;
    }
}





