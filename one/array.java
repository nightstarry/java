import java.util.Scanner;

public class array {
    public static void one(int[] mass)
    {
        for(int i = 0; i < mass.length; i++)
        {
            System.out.print(+mass[i]+ " ");
        }
    }

    public static void two(int[] mass)
    {
        for(int i = 0; i < mass.length; i++)
        {
            Scanner I = new Scanner(System.in);
            mass[i] = I.nextInt();
        }

    }
    public static int three (int[] mass)
    {

        int sum = 0;
        for(int i = 0; i < mass.length; i++)
        {
            sum += mass[i];
        } return sum;
    }
    public static int four(int[] mass)
    {
        int k = 0;
        for(int i = 0; i < mass.length; i++)
        {
            double s = mass[i];

            if (s%2 == 0)
            {
                k =  k + 1 ;
            }
        } return k;
    }
    public static int five(int[] mass, int a, int b)
    {

        int k = 0;
        for(int i = 0; i < mass.length; i++)
        {
            double s = mass[i];

            if ( (s <= b) && (s >= a))
            {
                k = k + 1;
            }
        } return k;
    }
    public static int six(int[] mass)
    {
        int s =0;
        int k = 0;
        for(int i = 0; i < mass.length; i++)
        {
            s = mass[i];

            if (s > 0)
            {
                k++;
            }
        } return k;
    }
    public static void seven(int[] mass)
    {
        for(int i = 0, j = mass.length - 1; i < mass.length/2; i++, j--)
        {
            int k = mass[i];
            mass[i] = mass[j];
            mass [j] = k;
        }
    }
    public static void main(String[] avg) {

        int menuchoice = 1;
        Scanner in = new Scanner(System.in);
        System.out.println(" Введите сколько хотите элементов в массиве:");
        int n = in.nextInt();
        int[] mass = new int[n];
        while (!(menuchoice == 15)) {
            System.out.println(" Выберите пункт:");
            System.out.println(" 8 - Вывод массива");
            System.out.println(" 9 - Ввод элементов с клавиатуры");
            System.out.println(" 10 - Сумма всех элементов");
            System.out.println(" 11 - Количество четных чисел");
            System.out.println(" 12 - Количество элементов, принадлежащих отрезку");
            System.out.println(" 13 - Все ли элементы положительные");
            System.out.println(" 14 - Перестановка элементов в обратном порядке");
            System.out.println(" 15 - Выход");
            menuchoice = in.nextInt();


            if (menuchoice == 8) {
                one(mass);
            }
            if (menuchoice == 9) {
                System.out.println(" Введите элементы массива:");
                System.out.println(" Массив:");
                two(mass);
            }
            if (menuchoice == 10) {
                System.out.println(" Введите элементы массива:");
                int sum = three(mass);
                System.out.print("Сумма всех элементов массива: " + sum);
            }
            if (menuchoice == 11) {
                System.out.println(" Введите элементы массива:");
                int k = four(mass);
                System.out.print("Количество четных элементов массива: " + k);
            }
            if (menuchoice == 12) {
                // k = 0;
                System.out.println(" Введите начало отрезка:");
                int a = in.nextInt();
                System.out.println(" Введите конец отрезка:");
                int b = in.nextInt();
                System.out.println(" Введите элементы массива:");
                int k = five(mass, a, b);
                System.out.print("Количество элементов массива, принадлежащие отрезку [" + a + ";" + b + "]:" + k);
            }
            if (menuchoice == 13) {
                //  int k=0;
                int k = six(mass);
                if (k != mass.length) {
                    System.out.print("Не все элементы массива положительные.");
                } else {
                    System.out.print("Все элементы массива положительные:");
                }
            }
            if (menuchoice == 14) {
                seven(mass);
            }
        }
    }
}
