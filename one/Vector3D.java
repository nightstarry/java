import java.util.Scanner;

public class Vector3D {
    private double x, y, z;

    public Vector3D() {
    }

    public void Vector3D(Point3D a, Point3D b) {
        x = b.getX() - a.getX();
        y = b.getY() - a.getY();
        z = b.getZ() - a.getZ();
    }


    public Vector3D(double x, double y, double z) {
        this.x = x;
        this.y = y;
        this.z = z;
    }


    public double getX() {
        return x;
    }

    public void setX(double x) {
        this.x = x;
    }

    public double getY() {
        return y;
    }

    public void setY(double y) {
        this.y = y;
    }

    public double getZ() {
        return z;
    }

    public void setZ(double z) {
        this.z = z;
    }

    public  double LengthVec() {
        return Math.sqrt(getX() * getX() + getY() * getY() + getZ() * getZ());
    }

    public static boolean equality(Vector3D one, Vector3D two) {
        double lengthvec = one.LengthVec();
        double lengthvec1 = two.LengthVec();
        System.out.println(+lengthvec1);
        boolean ok = false;
        boolean ok1;
        Vector3DProcessor Vector = new Vector3DProcessor();
        ok1 = Vector.collinerityVec(one, two);
        if ((lengthvec == lengthvec1) && ok1) {
            ok = true;
        }
        return ok;
    }

    public void PrintVector() {
        System.out.print("\tx =\t" + x + "\ty =\t" + y + "\tz =\t" + z);
    }


    public static void main(String[] avg) {
        Vector3DProcessor Vector = new Vector3DProcessor();
        Scanner in = new Scanner(System.in);
        int menu = 0, menu1 = 0;

        while (!(menu1 == 3)) {
            System.out.println("\t Выберите пункт:  ");
            System.out.println("1 - Vector3DProcessor");
            System.out.println("2 - vector3DArray");
            System.out.println("3 - выход");
            menu1 = in.nextInt();

            if (menu1 == 1) {
                System.out.println("Введите координаты первого вектора: ");
                double x1 = in.nextDouble();
                double y1 = in.nextDouble();
                double z1 = in.nextDouble();
                Vector3D one = new Vector3D(x1, y1, z1);
                System.out.println("Введите координаты второго вектора: ");
                double x2 = in.nextDouble();
                double y2 = in.nextDouble();
                double z2 = in.nextDouble();
                Vector3D two = new Vector3D(x2, y2, z2);

                while (!(menu == 8)) {

                    System.out.println("\t Выберите пункт: ");
                    System.out.println("1 - сумма векторов");
                    System.out.println("2 - разность векторов");
                    System.out.println("3 - скалярное произведение");
                    System.out.println("4 - коллинеарность");
                    System.out.println("5 - векторное произведение ");
                    System.out.println("6 - равны ли вектора");
                    System.out.println("7 - создать вектор по 2 точкам");
                    System.out.println("8 - выход");
                    menu = in.nextInt();


                    if (menu == 1) {
                        Vector3D sum = Vector.sumVec(one, two);
                        System.out.println("Сумма векторов = ");
                        sum.PrintVector();
                    }
                    if (menu == 2) {
                        Vector3D difference = Vector.differenceVec(one, two);
                        System.out.println("Разность векторов = ");
                        difference.PrintVector();
                    }
                    if (menu == 3) {
                        double scalarproduct = Vector.scalarProduct(one, two);
                        System.out.println("Скалярное произведение = " + scalarproduct);
                    }
                    if (menu == 4) {
                        boolean collinerity = Vector.collinerityVec(one, two);
                        if (collinerity == true) {
                            System.out.println("Векторы коллинеарны.");
                        } else {
                            System.out.println("Векторы не коллинеарны.");
                        }
                    }
                    if (menu == 5) {
                        Vector3D vectorproduct = Vector.vectorProduct(one, two);
                        System.out.println("Векторное произведение = ");
                        vectorproduct.PrintVector();
                    }
                    if (menu == 7) {

                        System.out.println("Введите координаты первой точки ");
                        double d = in.nextDouble();
                        double n = in.nextDouble();
                        double m = in.nextDouble();
                        Point3D a = new Point3D(d, n, m);
                        System.out.println("Введите координаты второй точки ");
                        double d1 = in.nextDouble();
                        double n1 = in.nextDouble();
                        double m1 = in.nextDouble();
                        Point3D b = new Point3D(d1, n1, m1);
                        Vector3D v = new Vector3D();
                        v.Vector3D(a, b);
                        v.PrintVector();
                    }
                    if (menu == 6) {
                        boolean ok = equality(one, two);
                        if (ok == true) {
                            System.out.println("Векторы равны");
                        } else {
                            System.out.println("Векторы не равны");
                        }
                    }

                    if (menu == 8) {
                        break;
                    }
                }
            }

            if (menu1 == 2) {
                int menu2 = 0;
                int size;
                System.out.print("\nВведите размер массива\n");
                size = in.nextInt();
                Vector3DArray array = new Vector3DArray(size);

                while (!(menu2 == 9)) {

                    System.out.println("\n Выберите пункт: ");
                    System.out.println("1 - вывод массив");
                    System.out.println("2 - длина массива");
                    System.out.println("3 - замена i-того элемента на заданный вектор");
                    System.out.println("4 - наибольшая длина вектора в массиве");
                    System.out.println("5 - поиск заданного вектора");
                    System.out.println("6 - сумма всех векторов в массиве");
                    System.out.println("7 - линейная комбинация");
                    System.out.println("8 - массив точек");
                    System.out.println("9 - выход");
                    menu2 = in.nextInt();

                    if (menu2 == 1) {

                        array.printArray();

                    }

                    if (menu2 == 2) {
                        double ok = array.LengthVector3DArray();
                        System.out.print("\nДлина массива = \n" + ok);
                    }

                    if (menu2 == 3) {
                        int o;
                        System.out.print("\nВведите индекс вектора\n");
                        o = in.nextInt();
                        System.out.print("\nЗадайте вектор для замены\n");
                        double a1 = in.nextDouble();
                        double b1 = in.nextDouble();
                        double c1 = in.nextDouble();
                        Vector3D six = new Vector3D(a1, b1, c1);
                        array.setVector(o, six);
                    }

                    if (menu2 == 4) {
                        double length = array.MaxLengthVector();
                        System.out.print("\nНаибольшая длина вектора:\n" + length);
                    }

                    if (menu2 == 5) {
                        System.out.print("\nВведите координаты вектора\n");
                        double a2 = in.nextDouble();
                        double b2 = in.nextDouble();
                        double c2 = in.nextDouble();
                        Vector3D six = new Vector3D(a2, b2, c2);
                        if (array.SearchVec(six) == -1) {
                            System.out.print("\nЗаданный вектор не найден\n");
                        } else {
                            System.out.print("\nИндекс вектора: " + array.SearchVec(six));
                        }
                    }

                    if (menu2 == 6) {
                        Vector3D sum = array.sumVectors();
                        System.out.print("\nСумма векторов:\n");
                        sum.PrintVector();
                    }

                    if (menu2 == 7) {
                        double[] arr = new double[size];
                        System.out.print("\nЗаполните массив:\n");
                        for (int i = 0; i < arr.length; i++) {
                            arr[i] = in.nextDouble();
                        }
                        Vector3D five = array.LinearCombination(arr);
                        System.out.print("\nРезультат:\n");
                        five.PrintVector();
                    }

                    if (menu2 == 8) {
                        {
                            System.out.print("\nВведите координаты точки:\n");
                            double a = in.nextDouble();
                            double b = in.nextDouble();
                            double c = in.nextDouble();
                            Point3D p = new Point3D(a, b, c);
                            System.out.print("\n Массив точек:\n");
                            Point3D arrayP[] = array.pointArray(p, size);
                            for (int i = 0; i < arrayP.length; i++) {
                                arrayP[i].printPoint();
                            }
                        }

                        if (menu2 == 9) {
                            break;
                        }
                    }
                }
            }

        }
    }

}
