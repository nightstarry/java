import java.util.Objects;
import java.util.Scanner;

public class Point3D {
    private double x, y, z;

    public Point3D() {
    }

    public Point3D(double x, double y, double z) {
        this.x = x;
        this.y = y;
        this.z = z;
    }

    public double getX() {
        return x;
    }

    public void setX(double x) {
        this.x = x;
    }

    public double getY() {
        return y;
    }

    public void setY(double y) {
        this.y = y;
    }

    public double getZ() {
        return z;
    }

    public void setZ(double z) {
        this.z = z;
    }

    public void printPoint() {
        System.out.print(this);
    }
    @Override
    public String toString() {
        return "(" +
                "x=" + x +
                ", y=" + y +
                ", z=" + z +
                ')';
    }
    //@Override



    public static boolean egualityPoint(Point3D one, Point3D two) {
        boolean ok = false;
        if ((Math.abs(one.getX() - two.getX()) <= 5e-10) && (Math.abs(one.getY() - two.getY()) <= 5e-10) && (Math.abs(one.getZ() - two.getZ()) <= 5e-10)) {
            ok = true;
        }
        return ok;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        Point3D point3D = (Point3D) o;
        return Double.compare(point3D.x, x) == 0 &&
                Double.compare(point3D.y, y) == 0 &&
                Double.compare(point3D.z, z) == 0;
    }

    @Override
    public int hashCode() {
        return Objects.hash(x, y, z);
    }

    public static void main(String[] avg) {
        Scanner in = new Scanner(System.in);
        System.out.println("Введите координаты первой точки: ");
        double x1 = in.nextDouble();
        double y1 = in.nextDouble();
        double z1 = in.nextDouble();
        Point3D one = new Point3D(x1, y1, z1);
        System.out.println("Введите координаты второй точки: ");
        double x2 = in.nextDouble();
        double y2 = in.nextDouble();
        double z2 = in.nextDouble();
        Point3D two = new Point3D(x2, y2, z2);
        System.out.println("\n Первая точка: "); one.printPoint();
        System.out.println("\n Вторая точка: "); two.printPoint();
        boolean eguality = egualityPoint(one, two);
        if (eguality) {
            System.out.println("\nТочки равны");
        } else {
            System.out.println("\nТочки не равны");
        }
        eguality = egualityPoint(one, one);
        if (eguality) {
            System.out.println("Точки равна сама себе");
        } else {
            System.out.println("Точки не равна сама себе");
        }

        two = one;
        if(one == two) { }

    }
}

