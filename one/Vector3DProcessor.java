

public  class Vector3DProcessor {

    public Vector3D sumVec(Vector3D one, Vector3D two) {
        // Vector3D sum = new Vector3D(one.getX() + two.getX(), one.getY() + two.getY(), one.getZ() + two.getZ());
        return  new Vector3D(one.getX() + two.getX(), one.getY() + two.getY(), one.getZ() + two.getZ());
    }

    public static Vector3D differenceVec(Vector3D one, Vector3D two) {
        Vector3D difference = new Vector3D(one.getX() - two.getX(), one.getY() - two.getY(), one.getZ() - two.getZ());
        return difference;
    }

    public static double scalarProduct(Vector3D one, Vector3D two) {
        double scalarproduct = one.getX() * two.getX() + one.getY() * two.getY() + one.getZ() * two.getZ();
        return scalarproduct;
    }

    public static Vector3D vectorProduct(Vector3D one, Vector3D two) {
        Vector3D vectorproduct = new Vector3D(one.getY() * two.getZ() - one.getZ() * two.getY(), one.getZ() * two.getX() - one.getX() * two.getZ(), one.getX() * two.getY() - one.getY() * two.getX());
        return vectorproduct;
    }

    public static boolean collinerityVec(Vector3D one, Vector3D two) {

        if ((Math.abs(vectorProduct(one, two).getX() - 0) <= 1e-10) && (Math.abs(vectorProduct(one, two).getY() - 0) <= 1e-10) && (Math.abs(vectorProduct(one, two).getZ() - 0) <= 1e-10)){
            return true;
        }
        return false;
    }
}

