import org.junit.Test;
import java.util.*;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.fail;

public class DemoTest {

    @Test(expected = HumanException.class)
    public void listTest1()throws HumanException{
        Human human = new Human("Иван", "Иванович", "Сидоров", -7);
        fail();
    }


    //1 задание
    //считает количество строк входного списка, у которых первый символ совпадает с заданным
    @Test()
    public void listTest2 (){
        List<String> list = new ArrayList<>();
        list.add("push");
        list.add("git");
        list.add("get");
        list.add("print");
        list.add("public");

        assertEquals(ListDemo.amountString(list, 'p'),3);
        assertEquals(ListDemo.amountString(list, 'g'), 2);
    }

    //2 задание
    //список однофамильцев
    @Test()
    public void listTest3 () throws HumanException {
        List <Human> list1 = new ArrayList<>();
        List <Human> list2 = new ArrayList<>();

        Human man = new Human("Иван", "Иванович", "Иванов", 20);

        //лист - результат
        list2.add(new Human("Иван", "Иванович", "Иванов", 21));
        list2.add(new Human("Иван", "Иванович", "Иванов", 22));

        //лист, который сравниваем
        list1.add(new Human("Иван", "Иванович", "Иванов", 21));
        list1.add(new Human("Иван", "Иванович", "Иванов", 22));
        list1.add(new Human("Иван", "Иванович", "Сидоров", 20));
        assertEquals(ListDemo.namesakes(list1, man), list2);
    }

    //3 задание
    //копия входного списка, не содержащая выделенного человека
    @Test
    public void listTest4()throws HumanException {
        List <Human> list1 = new ArrayList<>();
        List <Human> list2 = new ArrayList<>();

        Human man = new Human("Иван", "Иванович", "Иванов", 20);

        //лист - результат
        list2.add(new Human("Иван", "Иванович", "Иванов", 22));
        list2.add(new Human("Иван", "Иванович", "Сидоров", 20));

        //лист, который сравниваем
        list1.add(new Human("Иван", "Иванович", "Иванов", 20));
        list1.add(new Human("Иван", "Иванович", "Иванов", 22));
        list1.add(new Human("Иван", "Иванович", "Сидоров", 20));

        assertEquals(ListDemo.copyWithoutOne(list1, man), list2);
    }

    //4 задание
    //список всех множеств входного списка, которые не пересекаются с заданным множеством
    @Test
    public void listTest5(){
        List<Set<Integer>> set_list = new ArrayList<>();
        List<Set<Integer>> result_list = new ArrayList<>();
        Set<Integer> set = new HashSet<>();

        set.add(0);
        set.add(1);
        set.add(2);

        Set<Integer> set1 = new HashSet<>();

        set1.add(1);
        set1.add(4);
        set1.add(5);
        set_list.add(set1);

        Set<Integer> set2 = new HashSet<>();

        set2.add(6);
        set2.add(7);
        set2.add(8);
        set_list.add(set2);
        result_list.add(set2);

        assertEquals(result_list, ListDemo.disjointSets(set_list, set));
    }


    //5 задание
    //множество людей из входного списка с максимальным возрастом
    @Test
    public void listTest6() throws HumanException {
        List<Human> list = new ArrayList<>();
        Set<Human> result = new HashSet<>();

        list.add(new Human("Иван", "Иванович", "Иванов", 20));
        list.add(new Human("Иван", "Иванович", "Иванов", 22));
        list.add(new Human("Иван", "Иванович", "Сидоров", 22));
        result.add(new Human("Иван", "Иванович", "Иванов", 22));
        result.add(new Human("Иван", "Иванович", "Сидоров", 22));
        list.add(new Student("Иван", "Олегович", "Сидоров",19));

        assertEquals(result, ListDemo.maxAge(list));
    }

    //7 задание
    //множество людей, идентификаторы которых содержатся во входном множестве
    @Test
    public void listTest7()throws HumanException {
        Set<Integer> set = new HashSet<>();
        Set<Human> result = new HashSet<>();
        Map<Integer, Human> map = new HashMap<>();

        set.add(2);
        set.add(3);
        set.add(4);

        map.put(1, new Human("Иван", "Иванович", "Иванов", 20));
        map.put(2, new Human("Иван", "Иванович", "Сидоров", 22));
        result.add(new Human("Иван", "Иванович", "Сидоров", 22));

        assertEquals(result, ListDemo.humanWithId(map, set));
    }

    //8 задание
    //список идентификаторов людей, чей возраст не менее 18 лет
    @Test
    public void listTest8()throws HumanException {
        List<Integer> result = new ArrayList<>();
        Map<Integer, Human> map = new HashMap<>();

        map.put(1, new Human("Иван", "Иванович", "Иванов", 9));
        map.put(2, new Human("Иван", "Иванович", "Сидоров", 18));
        map.put(3, new Human("Иван", "Олегович", "Иванов", 1));
        map.put(5, new Human("Иван", "Олегович", "Сидоров", 19));

        result.add(2);
        result.add(5);

        assertEquals(result, ListDemo.ageLess(map));
    }

    //9 задание
    //новое отображение, которое идентификатору сопоставляет возраст человека
    @Test
    public void listTest9()throws HumanException {
        Map<Integer, Integer> result = new HashMap<>();
        Map<Integer, Human> map = new HashMap<>();

        map.put(1, new Human("Иван", "Иванович", "Иванов", 9));
        map.put(2,new Human("Иван", "Иванович", "Сидоров",18));
        map.put(3,new Human("Иван", "Олегович", "Иванов", 1));
        map.put(4,new Human("Иван", "Олегович", "Сидоров", 19));
        map.put(5, new Student("Иван", "Олегович", "Сидоров",19));

        result.put(1, 9);
        result.put(2, 18);
        result.put(3, 1);
        result.put(4, 19);
        result.put(5, 19);

        assertEquals(result, ListDemo.idWithAge(map));
    }

    //10 задание
    //по множеству объектов типа Human постройте отображение, которое возрасту человека сопоставляет список всех людей данного возраста из входного множества
    @Test
    public void listTest10()throws HumanException {
        Map<Integer, List<Human>> map = new HashMap<>();
        Set<Human> set = new HashSet<>();
        List<Human> list1 = new ArrayList<>();
        List<Human> list2 = new ArrayList<>();
        List<Human> list3 = new ArrayList<>();

        set.add(new Human("Иван", "Иванович", "Иванов", 9));
        set.add(new Human("Иван", "Иванович", "Сидоров",18));
        set.add(new Student("Иван", "Олегович", "Иванов",1));
        set.add(new Human("Иван", "Олегович", "Сидоров", 9));
        set.add(new Student("Олег", "Олегович", "Иванов",1));

        list1.add(new Human("Иван", "Иванович", "Иванов", 9));
        list1.add(new Human("Иван", "Олегович", "Сидоров", 9));
        list2.add(new Human("Иван", "Иванович", "Сидоров", 18));
        list3.add(new Student("Иван", "Олегович", "Иванов", 1));
        list3.add(new Student("Олег", "Олегович", "Иванов", 1));

        map.put(9, list1);
        map.put(18, list2);
        map.put(1, list3);

        assertEquals(map, ListDemo.ageWithSet(set));
    }
}
