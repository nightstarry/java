import java.util.*;

public class ListDemo {

    //1 задание
    //считает количество строк входного списка, у которых первый символ совпадает с заданным
    public static int amountString(List<String> list, char symbol) {
        int amount = 0;

        for (int i = 0; i < list.size(); i++) {
            if (list.get(i).charAt(0) == symbol) {
                amount++;
            }
        }
        return amount;
    }

    //2 задание
    //список однофамильцев
    public static List<Human> namesakes(List<Human> list, Human human) {
        List<Human> result = new ArrayList<>();

        for (int i = 0; i < list.size(); i++) {
            if (list.get(i).getLastName().equals(human.getLastName())) {
                result.add(list.get(i));
            }
        }
        return result;
    }

    //3 задание
    //копия входного списка, не содержащая выделенного человека
    public static List<Human> copyWithoutOne(List<Human> list, Human human) {
        List<Human> copyList = new ArrayList<>();

        for (int i = 0; i < list.size(); i++) {
            if (!list.get(i).equals(human)) {
                // copyList.add(list.get(i));
                copyList.add((new Human(list.get(i))));
            }
        }
        return copyList;
    }

    //4 задание
    //список всех множеств входного списка, которые не пересекаются с заданным множеством
    public static List<Set<Integer>> disjointSets(List<Set<Integer>> setList, Set<Integer> set) {
        List<Set<Integer>> result = new ArrayList<>();
        Iterator<Integer> it = set.iterator();

        for (int i = 0; i < setList.size(); i++) {
            while (it.hasNext()) {
                if (setList.get(i).contains(it.next())) {
                    break;
                }

                if (!it.hasNext()) {
                    result.add(setList.get(i));
                }
            }
            it = set.iterator();

            //set.retainAll()
        }
        return result;
    }

    //5 задание
    //множество людей из входного списка с максимальным возрастом
    public static Set<? extends Human> maxAge(List<? extends Human> list) {
        Set<Human> result = new HashSet<>();
        int maxAge = 0;

        for (int i = 0; i < list.size(); i++) {
            if (maxAge < list.get(i).getAge()) {
                result.clear();
                maxAge = list.get(i).getAge();
                result.add(list.get(i));
            }

            if (maxAge == list.get(i).getAge()) {
                result.add(list.get(i));
            }
        }
        return result;
    }

    //7 задание
    //множество людей, идентификаторы которых содержатся во входном множестве
    public static Set<Human> humanWithId(Map<Integer, Human> map, Set<Integer> set) {
        Set<Human> result = new HashSet<>();
        Iterator<Integer> it = set.iterator();
        int id;

        while (it.hasNext()) {
            id = it.next();

            if (map.get(id) != null) {
                result.add(map.get(id));
            }
        }
        return result;
    }

    //8 задание
    //список идентификаторов людей, чей возраст не менее 18 лет
    public static List<Integer> ageLess(Map<Integer, Human> map) {
        List<Integer> result = new ArrayList<>();
        Set<Integer> set = map.keySet();
        Iterator<Integer> it = set.iterator();
        int key;

        while (it.hasNext()) {
            key = it.next();

            if (map.get(key).getAge() >= 18) {
                result.add(key);
            }
        }

        return result;
    }

    //9 задание
    //новое отображение, которое идентификатору сопоставляет возраст человека
    public static Map<Integer, Integer> idWithAge(Map<Integer, Human> map) {
        Map<Integer, Integer> result = new HashMap<>();
        Set<Integer> set = map.keySet();
        Iterator<Integer> it = set.iterator();
        int key;

//        for(Map.Entry<Integer, Human> a : map.entrySet()) {
//            result.put(a.getKey(), a.getValue().getAge());
//        }

        while (it.hasNext()) {
            key = it.next();
            result.put(key, map.get(key).getAge());
        }

        return result;
    }

    //10 задание
    // по множеству объектов типа Human постройте отображение, которое возрасту человека сопоставляет список всех людей данного возраста из входного множества
    public static Map<Integer, List<Human>> ageWithSet(Set<Human> set) {
        Map<Integer, List<Human>> result = new HashMap<>();

        for (Human human : set) {
            if (!result.containsKey(human.getAge())) {
                result.put(human.getAge(), new ArrayList<Human>());
            }

            result.get(human.getAge()).add(human);
        }

        return result;
    }
}


