import com.fasterxml.jackson.databind.ObjectMapper;

import java.io.*;


public class HouseService {

    public static void serialization(House house, String fileName)  {
        try (ObjectOutputStream oos = new ObjectOutputStream(new FileOutputStream(fileName))) {
            oos.writeObject(house);
        } catch (IOException e) {
            e.printStackTrace();
        }
    }

    public static House deserialization(String fileName) {
        House house = new House();
        try (ObjectInputStream ois = new ObjectInputStream(new FileInputStream(fileName))) {
            house = (House) ois.readObject();
        } catch (IOException e) {
            e.printStackTrace();
        } catch (ClassNotFoundException e) {
            e.printStackTrace();
        }

        return house;
    }

    public static void serializationJackson(House house) throws IOException {

        ObjectMapper mapper = new ObjectMapper();
        mapper.writeValue(new File("3.json"), house);
    }

    public static House deserializationJackson() throws IOException {
        ObjectMapper mapper = new ObjectMapper();
        House house = mapper.readValue(new File("3.json"), House.class);
        return house;
    }


}
