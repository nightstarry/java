import java.io.Serializable;
import java.util.List;
import java.util.Objects;


public class House  implements Serializable {
    private String numHouse;
    private String address;
    private Person human;
    private List<Flat> flats;

    public House(String numHouse, String address, Person human, List<Flat> flats) {
        this.numHouse = numHouse;
        this.address = address;
        this.human = human;
        this.flats = flats;
    }
    public House(){}

    public String getNumHouse() {
        return numHouse;
    }

    public void setNumHouse(String numHouse) {
        this.numHouse = numHouse;
    }

    public String getAddress() {
        return address;
    }

    public void setAddress(String address) {
        this.address = address;
    }

    public Person getHuman() {
        return human;
    }

    public void setHuman(Person human) {
        this.human = human;
    }

    public List<Flat> getFlats() {
        return flats;
    }

    public void setFlats(List<Flat> flats) {
        this.flats = flats;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        House house = (House) o;
        return Objects.equals(numHouse, house.numHouse) &&
                Objects.equals(address, house.address) &&
                Objects.equals(human, house.human) &&
                Objects.equals(flats, house.flats);
    }

    @Override
    public int hashCode() {
        return Objects.hash(numHouse, address, human, flats);
    }

    @Override
    public String toString() {
        return "House{" +
                "numHouse='" + numHouse + '\'' +
                ", address='" + address + '\'' +
                ", human=" + human +
                ", flats=" + flats +
                '}';
    }
}
