import java.io.*;
import java.io.FileNotFoundException;
import java.util.ArrayList;
import java.util.List;


public class Stream {

    // запись в двоичный поток
    public static void writeStreamByte(String fileName, int[] array) {

        try (DataOutputStream dos = new DataOutputStream(new BufferedOutputStream(new FileOutputStream(fileName)))) {
            for (int i : array) {
                dos.writeInt(i);
            }

        } catch (FileNotFoundException e) {
            e.printStackTrace();
        } catch (IOException e) {
            e.printStackTrace();
        }

    }

    // чтение в двоичном потоке

    public static void readStreamByte(String fileName, int[] array) {

        try (DataInputStream dis = new DataInputStream(new BufferedInputStream(new FileInputStream(fileName)))) {

            for (int i = 0; i < array.length; i++) {
                array[i] = dis.readInt();
            }

        } catch (FileNotFoundException e) {
            e.printStackTrace();
        } catch (IOException e) {
            e.printStackTrace();
        }
    }

    // запись в символьнм потоке
    public static void writeStreamSymbol(String fileName, int[] array) throws IOException {
        try (FileWriter fw = new FileWriter(fileName)){
            String s = "";
            for(int i:array){
                s = s + " " + i;
            }
            fw.write(s);
        }
    }

    // чтение в символьном потоке
    public static int[] readStreamSymbol(String fileName, int[] array)throws IOException {
        try (BufferedReader br = new BufferedReader(new FileReader(fileName)) ) {
            String[] e = br.readLine().trim().split(" ");
            for (int i = 0; i < array.length; i++) {
                array[i] = Integer.parseInt(e[i]);
            }
            br.close();
            return array;
        }
    }


    //чтение с заданной позиции
    public static int[] readRandomAccessFile(String fileName, int[] array, int pos) {
            try {
            RandomAccessFile file = new RandomAccessFile(fileName, "r");
            file.seek(4*pos);
            for(int i = pos; i < array.length; i++){
                array[i] = file.readInt();
            }

        } catch (FileNotFoundException e) {
            e.printStackTrace();
        } catch (IOException e) {
            e.printStackTrace();
        }
        return array;
    }

    // получение списка всех файлов с заданным расширением в заданном каталоге
    public static List<String> getFileByExtension(String name, String s) {
        List<String> result = new ArrayList<>();
        File dir = new File(name);

        if (dir.isDirectory()) {
            for (File item : dir.listFiles()) {
                String str = item.getName();

                if (str.endsWith(s)) {
                    result.add(item.getName());
                }
            }
        }

        return result;
    }
}

