import java.io.*;
import java.util.ArrayList;


public class Main {

    public static void main(String avg[]) throws IOException, ClassNotFoundException {

        Person person = new Person("Иван","Иванович", "Иванов", 23);
        ArrayList<Flat> flats = new ArrayList<>();
        flats.add(new Flat());
        flats.add(new Flat());
        House house = new House("55", "Мира 55", person, flats);

        String fileName = "3.txt";

        System.out.println("Сериализация/десериализация средствами Java: ");
        HouseService.serialization(house, fileName);
        System.out.println(HouseService.deserialization(fileName).toString());
        System.out.println("Сериализация/десериализация Jackson: ");

        try {
            HouseService.serializationJackson(house);
        } catch (IOException e) {
            e.printStackTrace();
        }

        try {
            System.out.println(HouseService.deserializationJackson().toString());
        } catch (IOException e) {
            e.printStackTrace();
        }
    }

}

