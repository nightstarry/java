import org.junit.Test;
import java.io.*;
import java.util.*;

import static org.junit.Assert.assertArrayEquals;
import static org.junit.Assert.assertEquals;

public class Test1 {

    @Test()
    public void TestByteStream(){
        int []arr = {1, 2, 3};
        int []a = new int[3];
        String name = "1.txt";

        Stream.writeStreamByte(name, arr);

        Stream.readStreamByte(name, a);
    }

    @Test()
    public void TestSymbolStream() throws IOException {
        int []arr = {1, 2, 3};
        int []a = new int[3];
        String name = "2.txt";

        Stream.writeStreamSymbol(name, arr);

        Stream.readStreamSymbol(name, a);

        assertArrayEquals(arr, a);
    }

    @Test()
    public void TestRandomAccessFile(){
        int []a = new int[3];
        int []b = new int[3];
        b[2] = 3;

        Stream.readRandomAccessFile("1.txt", a, 2);

        assertArrayEquals(b,a);
    }

    @Test()
    public void TestByExtension(){
        String name = "C:\\\\Users\\\\space\\\\Desktop\\\\универ\\\\4 СЕМЕСТР кроме one\\\\three4";
        String extension = ".txt";
        List<String> list = new ArrayList<>();

        list.add("1.txt");
        list.add("2.txt");
        list.add("3.txt");
        list.add("s.txt");

        assertEquals(Stream.getFileByExtension(name, extension), list);
    }
}
