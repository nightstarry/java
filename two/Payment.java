
import java.lang.String;
import java.util.Objects;


public class Payment {
    private String FIO;
    private int day, month, year/*дата платежа*/, sum /*в копейках*/;

    public Payment() {
    }

    public Payment(int d, int m, int y, String fio, int s) {
        this.day = d;
        this.month = m;
        this.year = y;
        this.FIO = fio;
        this.sum = s;
    }

    public String getFIO() {
        return FIO;
    }

    public void setFIO(String FIO) {
        this.FIO = FIO;
    }

    public int getDay() {
        return day;
    }

    public void setDay(int day) {
        this.day = day;
    }

    public int getMonth() {
        return month;
    }

    public void setMonth(int month) {
        this.month = month;
    }

    public int getYear() {
        return year;
    }

    public void setYear(int year) {
        this.year = year;
    }

    public int getSum() {
        return sum;
    }

    public void setSum(int sum) {
        this.sum = sum;
    }

    @Override
    public boolean equals(Object obj) {
        if (this == obj) return true;
        if (obj == null || getClass() != obj.getClass()) return false;
        Payment payment = (Payment) obj;
        return day == payment.day && month == payment.month && year == payment.year && sum == payment.sum && Objects.equals(FIO, payment.FIO);
    }

    @Override
    public int hashCode() {
        return Objects.hash(FIO, day, month, year, sum);
    }

    @Override
    public String toString() {
        return "main.Payment{" + "ФИО: '" + FIO + '\'' + ", день: " + day + ", месяц: " + month + ", год: " + year + ", сумма = " + sum + '}';
    }
}
