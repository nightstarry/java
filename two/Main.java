import java.util.Scanner;
import java.lang.String;

public class Main {
    public static void main(String[] args) {
        Scanner S = new Scanner(System.in);
        int menu = 0;
        while(menu!=8) {
            System.out.println("1 - копия строки");
            System.out.println("2 - количество вхождений второй строки в первую");
            System.out.println("3 - замена");
            System.out.println("4 - удаление каждого второго символа");
            System.out.println("5 - класс Payment");
            System.out.println("6 - 8 задание");
            System.out.println("7 - 9 задание");
            System.out.println("8 - Выход ");
            menu = S.nextInt();
            S.nextLine();
            StringProcessor n1 = new StringProcessor();
            if (menu == 1) {
                System.out.println("Введите строку:");
                String s = S.nextLine();
                System.out.println("Введите N:");
                int N = S.nextInt();
                System.out.println("Результат:");
                String s1 = new String();
                s1 = n1.StringCopy(s, N);
                System.out.println(s1);

            }
            if (menu == 2) {
                System.out.println("Введите строку 1:");
                String s1 = S.nextLine();
                System.out.println("Введите строку 2:");
                String s2 = S.nextLine();
                System.out.println("Результат:");
                System.out.println(n1.quantityOfEntries(s1, s2));
            }
            if (menu == 3) {
                System.out.println("Введите строку:");
                String s = S.nextLine();
                System.out.println("Результат:");
                System.out.println(n1.StringChange(s));
            }
            if (menu == 4) {
                System.out.println("Введите строку:");
                StringBuilder s = new StringBuilder(S.nextLine());
                System.out.println("Результат:");
                System.out.println(n1.delete(s));
            }
            if (menu == 5) {

                System.out.println("Введите ФИО:");
                String fio = S.nextLine();
                System.out.println("Введите день, месяц, год платежа:");
                int d = S.nextInt();
                int m = S.nextInt();
                int y = S.nextInt();
                System.out.println("Введите сумму платежа:");
                int s = S.nextInt();
                Payment pay1 = new Payment(d, m, y, fio, s);
                System.out.println(pay1.toString());
                S.nextLine();
                System.out.println("Введите ФИО :");
                fio = S.nextLine();
                System.out.println("Введите день, месяц, год платежа:");
                d = S.nextInt();
                m = S.nextInt();
                y = S.nextInt();
                System.out.println("Введите сумму платежа:");
                s = S.nextInt();
                Payment pay2 = new Payment(d, m, y, fio, s);
                System.out.println(pay2.toString());
                System.out.println(pay1.equals(pay2));
                System.out.println(pay1.hashCode());
                System.out.println(pay2.hashCode());
            }
            if (menu == 6) {
                System.out.println("Введите размер массива:");
                int size = S.nextInt();
                FinanceReport array = new FinanceReport(size);
                String fio;
                int d, m, y, s;
                for (int i = 0; i < size; i++) {
                    System.out.println("Введите день, месяц, год платежа:");
                    d = S.nextInt();
                    m = S.nextInt();
                    y = S.nextInt();
                    S.nextLine();
                    System.out.println("Введите ФИО:");
                    fio = S.nextLine();
                    System.out.println("Введите сумму платежа:");
                    s = S.nextInt();
                    array.writePayment(i, fio, d, m, y, s);
                }
                int minimenu = 0;
                while (minimenu != 5) {
                    System.out.println("1 - Получение количества платежей ");
                    System.out.println("2 - Доступ к i-му платежу на чтение ");
                    System.out.println("3 - Доступ к i-му платежу на запись ");
                    System.out.println("4 - Показать массив платежей ");
                    System.out.println("5 - Выход ");
                    minimenu = S.nextInt();
                    if (minimenu == 1) {
                        System.out.println(array.getSize());
                    }
                    if (minimenu == 2) {
                        System.out.println("Введите индекс массива:");
                        int i = S.nextInt();
                        System.out.println(array.readPayment(i));
                    }
                    if (minimenu == 3) {
                        S.nextLine();
                        System.out.println("Введите индекс массива:");
                        int i = S.nextInt();
                        S.nextLine();
                        System.out.println("Введите ФИО:");
                        fio = S.nextLine();
                        System.out.println("Введите день, месяц, год платежа:");
                        d = S.nextInt();
                        m = S.nextInt();
                        y = S.nextInt();
                        System.out.println("Введите сумму платежа:");
                        s = S.nextInt();
                        array.writePayment(i, fio, d, m, y, s);
                    }
                    if (minimenu == 4) {
                        for (int i = 0; i < size; i++) {
                            System.out.println(array.readPayment(i));
                        }
                    }
                }
            }
            if (menu == 7) {
                System.out.println("Введите размер массива:");
                int size = S.nextInt();
                FinanceReport array = new FinanceReport(size);
                String fio;
                int d, m, y, s;
                FinanceReport cop_arr = new FinanceReport(size);
                for (int i = 0; i < size; i++) {
                    S.nextLine();
                    System.out.println("Введите ФИО:");
                    fio = S.nextLine();
                    System.out.println("Введите день, месяц, год платежа:");
                    d = S.nextInt();
                    m = S.nextInt();
                    y = S.nextInt();
                    System.out.println("Введите сумму платежа:");
                    s = S.nextInt();
                    array.writePayment(i, fio, d, m, y, s);
                }
                int minimenu = 0;
                while (minimenu != 6) {
                    System.out.println("1 - Инф-ия о платежах людей, чья фамилия начинается на заданный символ");
                    System.out.println("2 - Конструктор копирования ");
                    System.out.println("3 - Вывод платежей, размер которых меньшее заданной величины ");
                    System.out.println("4 - Показать массив платежей ");
                    System.out.println("5 - Показать копию массива платежей ");
                    System.out.println("6 - Выход ");
                    minimenu = S.nextInt();

                    if (minimenu == 1) {
                        S.nextLine();
                        System.out.println("Введите символ:");
                        String c = S.nextLine();
                        array.searchByLastName(c);
                    }
                    if (minimenu == 2) {

                        cop_arr.copyFinanceReport(array);
                    }
                    if (minimenu == 3) {
                        S.nextLine();
                        System.out.println("Введите величину:");
                        int value = S.nextInt();
                        array.PaymentLength(value);
                    }
                    if (minimenu == 4) {
                        for (int i = 0; i < size; i++) {
                            System.out.println(array.readPayment(i));
                        }
                    }
                    if (minimenu == 5) {
                        for (int i = 0; i < size; i++) {
                            System.out.println(cop_arr.readPayment(i));
                        }
                    }
                }

            }
        }
    }
}





