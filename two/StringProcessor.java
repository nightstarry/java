
import java.lang.String;
import java.lang.StringBuilder;


public class StringProcessor {

    public StringProcessor(){}

    public static String StringCopy(String s, int N){
        if(N < 0){
            throw new IllegalArgumentException("Число N отрицательное");
        }

        String s1= "";
        int m = 0;
        while (m != N) {
            s1 += s;
            m++;
        }
        return s1;
    }

    public static int quantityOfEntries(String s1, String s2){
        int k = -1;
        int i = 0;
        int x = -1;
        while(i!=-1){
            i = s1.indexOf(s2,x + 1);
            x = i;
            k++;
        }
        return k;
    }

    public static String StringChange(String s1){
        s1 = s1.replace("1","один");
        s1 = s1.replace("2","два");
        s1 = s1.replace("3","три");
        return s1;
    }

    public static StringBuilder delete(StringBuilder s) {
        for (int i = 1; i < s.length(); i += 1) {
            s.delete(i, i + 1);
        }
        return s;
    }
}



