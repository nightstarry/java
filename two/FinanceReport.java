
public class FinanceReport {
    private Payment[] array;

    public FinanceReport(int size){
        array = new Payment[size];
        for (int i = 0; i < size; i++) {
            array[i] = new Payment();
        }
    }

    public int getSize() {
        return array.length;
    }

    public void writePayment(int i, String FIO, int day, int month, int year, int sum){
        array[i] = new Payment(day, month, year, FIO,  sum);
    }

    public Payment readPayment(int i){
        return array[i];
    }


    public void searchByLastName(String s){
        String result;
        for(Payment p: array){
            if (p.getFIO().startsWith(s)){
                result = String.format("Плательщик: %S , дата: %d.%d.%d , сумма: %d руб. %d коп. " ,  p.getFIO(), p.getDay(), p.getMonth(), p.getYear(),
                        p.getSum()/100, p.getSum()%100 );
                System.out.println(result);
            }
        }
    }

    public void copyFinanceReport(FinanceReport original){
        array = new Payment[original.getSize()];
        for (int i = 0; i < array.length; i++){
            array[i] = new Payment(
                    original.readPayment(i).getDay(),
                    original.readPayment(i).getMonth(),
                    original.readPayment(i).getYear(),
                    original.readPayment(i).getFIO(),
                    original.readPayment(i).getSum());
        }
    }

    public void PaymentLength(int sum){
        String result;
        for(Payment p: array){
            if (p.getSum() < sum){
                result = String.format("Плательщик: %S , дата: %d.%d.%d , сумма: %d руб. %d коп. " ,  p.getFIO(), p.getDay(), p.getMonth(), p.getYear(),
                        (int)Math.floor(p.getSum()/100), p.getSum()%100 );
                System.out.println(result);
            }
        }
    }

}


